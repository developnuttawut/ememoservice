﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EMEMOSERVICE
{
    public static class LogHelper
    {
        private static readonly string fullPath = "C:\\inetpub\\wwwroot\\Smartlife\\EMEMO_API\\Service\\Log";
        public static void InsertLogInfo(string message)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath); //Create directory if it doesn't exist
            }

            File.AppendAllText($"{fullPath}\\EMEMOSERVICELOG_{DateTime.Now.ToString("yyyy-MM-dd")}.txt",
           $"INFO {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} {message} {Environment.NewLine}");
        }

        public static void InsertLogError(Exception ex)
        {
            if (!Directory.Exists(fullPath))
            {
                Directory.CreateDirectory(fullPath); //Create directory if it doesn't exist
            }

            string errorData = JsonConvert.SerializeObject(ex);
            File.AppendAllText($"{fullPath}\\EMEMOSERVICELOG_{DateTime.Now.ToString("yyyy-MM-dd")}.txt",
           $"ERROR {DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss")} {errorData} {Environment.NewLine}");
        }
    }
}
