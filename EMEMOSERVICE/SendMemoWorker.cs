﻿using EMEMOSERVICE.DBContext;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EMEMOSERVICE
{
    public class SendMemoWorker : BackgroundService
    {
        private readonly Repository _repository;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(SendMemoWorker));
        public SendMemoWorker(IServiceScopeFactory factory)
        {
            _repository = factory.CreateScope().ServiceProvider.GetRequiredService<Repository>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _repository.AutoSendMemo();
                }
                catch (Exception ex)
                {
                    LogHelper.InsertLogError(ex);
                }

                var configData = await _repository.GetConfigBy(c => c.Config_Name == "TimeSendMemo");
                int timeDelay = 30;
                if(configData != null)
                {
                    timeDelay = int.Parse(configData.Value);
                }

                await Task.Delay(TimeSpan.FromMinutes(timeDelay), stoppingToken);
            }
        }
    }
}
