﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EMEMOSERVICE.DBContext
{
    public class TMConfig
    {
        [Key]
        public int ID { get; set; }
        public string Config_Name { get; set; }
        public string Value { get; set; }
    }
}
