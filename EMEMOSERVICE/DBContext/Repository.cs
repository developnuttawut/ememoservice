﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace EMEMOSERVICE.DBContext
{
    public class Repository
    {
        private readonly eMemoDBContext _dbContext;
        private readonly string emeoAPIUrl = "http://10.5.12.130/EMEMO_API";
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(Repository));
        public Repository(eMemoDBContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<bool> AutoRetrieveTransaction()
        {
            try
            {
                LogHelper.InsertLogInfo("AutoRetrieveTransaction");
                using (var httpClientHandler = new HttpClientHandler())
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    using (var client = new HttpClient(httpClientHandler))
                    {
                        var response = await client.GetAsync($"{emeoAPIUrl}/Transaction/RetrieveTransaction");
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }
        }

        public async Task<bool> AutoUpdateTransaction()
        {
            try
            {
                LogHelper.InsertLogInfo("AutoUpdateTransaction");
                using (var httpClientHandler = new HttpClientHandler())
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    using (var client = new HttpClient(httpClientHandler))
                    {
                        var response = await client.GetAsync($"{emeoAPIUrl}/Transaction/UpdateMemoTransaction");
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }
        }

        public async Task<bool> AutoSendMemo()
        {
            try
            {
                LogHelper.InsertLogInfo("AutoSendMemo");
                using (var httpClientHandler = new HttpClientHandler())
                {
                    httpClientHandler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; };
                    using (var client = new HttpClient(httpClientHandler))
                    {
                        var response = await client.GetAsync($"{emeoAPIUrl}/Transaction/AutoSendMemo");
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                return false;
            }
        }

        public async Task<TMConfig> GetConfigBy(Expression<Func<TMConfig, bool>> expression)
        {
            try
            {
                return await _dbContext.TMConfig.Where(expression).AsNoTracking().FirstOrDefaultAsync();

            }
            catch (Exception ex)
            {
                _log4net.Error(JsonConvert.SerializeObject(ex));
                throw ex;
            }
        }

    }
}
