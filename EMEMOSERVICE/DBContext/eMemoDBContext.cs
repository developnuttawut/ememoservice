﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace EMEMOSERVICE.DBContext
{
    public class eMemoDBContext : DbContext
    {
        public eMemoDBContext(DbContextOptions<eMemoDBContext> options) : base(options)
        {

        }

        public DbSet<TMConfig> TMConfig { get; set; }
    }
}
