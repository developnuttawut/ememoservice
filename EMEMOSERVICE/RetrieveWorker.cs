using EMEMOSERVICE.DBContext;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;

namespace EMEMOSERVICE
{
    public class RetrieveWorker : BackgroundService
    {
        private readonly Repository _repository;
        static readonly log4net.ILog _log4net = log4net.LogManager.GetLogger(typeof(RetrieveWorker));
        public RetrieveWorker(IServiceScopeFactory factory)
        {
            _repository = factory.CreateScope().ServiceProvider.GetRequiredService<Repository>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _repository.AutoRetrieveTransaction();
                    _repository.AutoUpdateTransaction();
                }
                catch (Exception ex)
                {
                    LogHelper.InsertLogError(ex);

                }

                var configData = await _repository.GetConfigBy(c => c.Config_Name == "TimeRetrieve");
                int timeDelay = 10;
                if (configData != null)
                {
                    timeDelay = int.Parse(configData.Value);
                }

                await Task.Delay(TimeSpan.FromMinutes(timeDelay), stoppingToken);
            }
        }
    }
}
