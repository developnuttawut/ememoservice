using EMEMOSERVICE.DBContext;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace EMEMOSERVICE
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                LogHelper.InsertLogInfo("Service Start");

                CreateHostBuilder(args).Build().Run();
            }
            catch(Exception ex)
            {
                LogHelper.InsertLogError(ex);
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<RetrieveWorker>();
                    services.AddHostedService<SendMemoWorker>();
                    services.AddScoped<Repository>();
                    services.AddDbContext<eMemoDBContext>(
                o => o.UseSqlServer("Data Source=10.5.12.134;Initial Catalog=EMEMO;User Id=usrememo01;Password=ememo@2022tgh"));
                }).UseWindowsService();
    }
}
